#! /usr/bin/python
# -*- coding:Utf-8 -*-

from setuptools import setup
from ppastatslib.constants import VERSION

import os
import sys


if 'linux' in sys.platform:
    images = [os.path.join("ppastatslib/images/", img) for img in os.listdir("ppastatslib/images/")]
    data_files = [("/usr/share/applications", ["ppastats.desktop"]),
                  ("/usr/share/ppastats/images/", images),
                  ("/usr/share/doc/ppastats/", ["README.rst"]),
                  ("/usr/share/man/man1", ["ppastats.1.gz"]),
                  ("/usr/share/locale/en_US/LC_MESSAGES/", ["ppastatslib/locale/en_US/LC_MESSAGES/ppastats.mo"]),
                  ("/usr/share/locale/fr_FR/LC_MESSAGES/", ["ppastatslib/locale/fr_FR/LC_MESSAGES/ppastats.mo"]),
                  ("/usr/share/pixmaps", ["ppastats.svg"])]
    package_data = {}
else:
    data_files = []
    package_data = {'ppastatslib': ['images/*',
                                    'locale/fr_FR/LC_MESSAGES/ppastats.mo',
                                    'locale/en_US/LC_MESSAGES/ppastats.mo']}


setup(name="ppastats",
      version=VERSION,
      description="Launchpad PPA stats viewer",
      author="Juliette Monsel",
      author_email="j_4321@protonmail.com",
      license="GPLv3",
      url="https://gitlab.com/j_4321/ppastats/",
      packages=['ppastatslib'],
      scripts=["ppastats"],
      data_files=data_files,
      package_data=package_data,
      classifiers=[
            'Development Status :: 5 - Production/Stable',
            'Intended Audience :: Developers',
            'Operating System :: OS Independent',
            'Topic :: Software Development',
            'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
            'Programming Language :: Python :: 3',
            'Programming Language :: Python :: 3.5',
            'Programming Language :: Python :: 3.6',
            'Natural Language :: English',
            'Natural Language :: French'
      ],
      long_description=
"""
ppastats displays the download statistics of PPAs from Launchpad API.
""",
      install_requires=["pandas", "matplotlib", "pytables", "launchpadlib"])
