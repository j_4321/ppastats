ppastats
========
Launchpad PPA stats viewer
--------------------------
Copyright (C) 2018  Juliette Monsel <j_4321@protonmail.com>


ppastats displays the download statistics of PPAs from Launchpad API.


Prerequisite
~~~~~~~~~~~~

    python3 librairies tkinter, matplotlib, pandas and launchpadlib 

Launch
~~~~~~

::

    python3 ppastats

Install
~~~~~~~

::

    sudo python3 setup.py install
