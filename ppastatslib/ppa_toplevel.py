#! /usr/bin/python3
# -*- coding: utf-8 -*-
"""
ppastats - Launchpad PPA stats viewer
Copyright 2018 Juliette Monsel <j_4321@protonmail.com>

ppastats is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ppastats is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Toplevel to display PPA's stats
"""
import tkinter as tk
from ppastatslib.messagebox import showerror
from tkinter import ttk
from ppastatslib.constants import fetch_ppa_stats, PATH_DATA, IM_UPDATE
from ppastatslib.page import Page
from ppastatslib.scrolltabnotebook import ScrollTabNotebook as Notebook
import os
import pandas as pd
import logging
from threading import Thread
from lazr.restfulclient import errors
import traceback
from webbrowser import open as webopen


class PPAToplevel(tk.Toplevel):
    def __init__(self, ppa_owner, ppa_name, distributions, archs):
        tk.Toplevel.__init__(self)
        self.protocol("WM_DELETE_WINDOW", self.quit)

        self.owner = ppa_owner
        self.name = ppa_name
        self.distributions = distributions
        self.archs = archs

        self._after_id = ''
        self._im_update = tk.PhotoImage(master=self, file=IM_UPDATE)

        self.title('{}/{}'.format(ppa_owner, ppa_name))

        self.notebook = Notebook(self, closebutton=False)

        url = 'https://launchpad.net/~{}/+archive/ubuntu/{}'.format(self.owner, self.name)
        link = ttk.Label(self, style='url.TLabel', text=url, cursor='hand1',
                         font='TkDefaultFont 9 underline')
        link.bind('<1>', lambda e: webopen(url))

        self.pages = {}
        self.pages['PPA'] = Page(self.notebook, pagetype='PPA')
        self.notebook.add(self.pages['PPA'], text='PPA')

        self.thread = Thread()

        self.path_data_saved = os.path.join(PATH_DATA, '{}_{}.h5'.format(ppa_owner, ppa_name))

        self.data_tot = pd.DataFrame(columns=['Package', 'Version', 'Downloads'])
        self.data_downloads = {}
        if os.path.exists(self.path_data_saved):
            try:
                self.data_tot = pd.read_hdf(self.path_data_saved, 'tot')
                for pkg in self.data_tot.groupby('Package').groups:
                    self.data_downloads[pkg] = pd.read_hdf(self.path_data_saved, pkg.replace('-', '_'))
            except KeyError:
                pass
            except ValueError:
                showerror(_('Error'), _('Invalid data file {file_path}').format(file_path=self.path_data_saved))

        self.notebook.pack(fill='both', expand=True)

        link.pack(side='left', padx=4, pady=4)
        ttk.Button(self, text=_('Update'), image=self._im_update,
                   compound='right', padding=2,
                   command=self.launch_update).pack(side='right', padx=4, pady=4)
        self.show_data()
        self.launch_update()

    def quit(self):
        try:
            self.after_cancel(self._after_id)
        except ValueError:
            pass
        self.destroy()

    def _fetch_data(self):
        try:
            self.data_tot, self.data_downloads = fetch_ppa_stats(self.owner,
                                                                 self.name,
                                                                 self.distributions,
                                                                 self.archs)
        except errors.NotFound:
            showerror(_('Error'),
                      _('Invalid PPA configuration for {ppa_owner}/{ppa_name}').format(ppa_owner=self.owner, ppa_name=self.name))
            self.data_tot = pd.DataFrame(columns=['Package', 'Version', 'Downloads'])
            self.data_downloads = {}
            logging.error('Invalid PPA configuration for {}/{}'.format(self.owner, self.name))
        except Exception as e:
            logging.exception(str(e))
            self.data_tot = pd.DataFrame(columns=['Package', 'Version', 'Downloads'])
            self.data_downloads = {}
            showerror(_('Error'), '{}: {}'.format(str(type(e)), str(e)), traceback.format_exc())
        else:
            logging.info('Updated data for PPA {}/{}'.format(self.owner, self.name))

    def tight_layout(self):
        tab = self.notebook.tab(self.notebook.current_tab, 'text')
        self.pages[tab].tight_layout()

    def launch_update(self):
        logging.info('Launching update for PPA {}/{}'.format(self.owner, self.name))
        self.thread = Thread(target=self._fetch_data)
        self.thread.daemon = True
        self.thread.start()
        self._after_id = self.after(100, self.update_data)

    def update_data(self):
        if self.thread.is_alive():
            self._after_id = self.after(100, self.update_data)
        else:
            self.data_tot.to_hdf(self.path_data_saved, 'tot')
            for pkg, data in self.data_downloads.items():
                data.to_hdf(self.path_data_saved, pkg.replace('-', '_'))
            self.show_data()

    def show_data(self):
        pkgs = self.data_tot.groupby('Package')
        downloads = pd.DataFrame(columns=['Downloads'])

        for pkg in pkgs.groups:
            if pkg not in self.pages:
                self.pages[pkg] = Page(self.notebook)
                self.notebook.add(self.pages[pkg], text=pkg)
            pkgdata = pkgs.get_group(pkg)
            pkgdata.index = pkgdata['Version']
            pkgdownloads = self.data_downloads[pkg]
            downloads = downloads.add(pkgdownloads, fill_value=0)
            self.pages[pkg].plot(pkgdata, pkgdownloads)

        self.pages['PPA'].plot(pkgs.sum().sort_values('Downloads'), downloads)
