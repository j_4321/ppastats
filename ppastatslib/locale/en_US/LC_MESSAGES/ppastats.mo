��    !      $  /   ,      �     �     �          !     (  D   .  :   s     �     �  (   �  	   �     �            4        H     f     o     �     �  	   �  
   �     �  F   �  9        F     S     i     p  �   x  �   �  �   �  X  �     '	     6	     P	     _	     f	  D   l	  :   �	     �	     �	  (   
  	   0
     :
     @
     E
  4   Q
     �
     �
     �
     �
     �
  	   �
  
   �
     �
  F     9   J     �     �     �     �  �   �  �   ;  �                                                                        
      !                                                     	                                About ppastats All fields are mandatory. Architectures: Cancel Close Comma separated list of Ubuntu distributions, e.g. "xenial, bionic". Comma separated list of architectures, e.g. "amd64, i386". Configure PPA Confirmation Do you really want to delete {ppa_name}? Downloads Error Help Information Invalid PPA configuration for {ppa_owner}/{ppa_name} Invalid data file {file_path} Language Launchpad PPA stats viewer License Ok PPA name: PPA owner: Please report this bug on  The language setting will take effect after restarting the application This PPA is already configured. Do you want to modify it? Tight layout Total downloads: {nb} Update Warning You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/. ppastats is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. ppastats is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. Project-Id-Version: 1.0.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-11-06 16:25+0100
PO-Revision-Date: 2018-11-06 11:00+0100
Last-Translator:  <j_4321@protonmail.com>
Language-Team: English
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 About ppastats All fields are mandatory. Architectures: Cancel Close Comma separated list of Ubuntu distributions, e.g. "xenial, bionic". Comma separated list of architectures, e.g. "amd64, i386". Configure PPA Confirmation Do you really want to delete {ppa_name}? Downloads Error Help Information Invalid PPA configuration for {ppa_owner}/{ppa_name} Invalid data file {file_path} Language Launchpad PPA stats viewer License Ok PPA name: PPA owner: Please report this bug on  The language setting will take effect after restarting the application This PPA is already configured. Do you want to modify it? Tight layout Total downloads: {nb} Update Warning You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/. ppastats is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. ppastats is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. 