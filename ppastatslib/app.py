#! /usr/bin/python3
# -*- coding: utf-8 -*-
"""
ppastats - Launchpad PPA stats viewer
Copyright 2018 Juliette Monsel <j_4321@protonmail.com>

ppastats is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ppastats is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Main window
"""
import tkinter as tk
from tkinter import ttk
from tkinter.font import Font
from ppastatslib.messagebox import showerror, askokcancel, showinfo
from ppastatslib.constants import ICON, save_config, CONFIG, PATH_DATA, \
    IM_ADD, IM_DEL, IM_EDIT, IM_STATS, IM_ABOUT, IM_HELP, SETTINGS, \
    save_settings, LANGUAGES
from ppastatslib.ppa_toplevel import PPAToplevel
from ppastatslib.about import About
from ppastatslib.help import Help
from ppastatslib.utils import AutoScrollbar
import configparser
import logging
import traceback
import os
import matplotlib


class App(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self, className='ppastats')
        logging.info('Starting ppastats')
        self.minsize(239, 114)
        self.columnconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)

        self.protocol('WM_DELETE_WINDOW', self.quit)
        self._icon = tk.PhotoImage(master=self, file=ICON)
        self.iconphoto(True, self._icon)

        self._im_add = tk.PhotoImage(master=self, file=IM_ADD)
        self._im_del = tk.PhotoImage(master=self, file=IM_DEL)
        self._im_edit = tk.PhotoImage(master=self, file=IM_EDIT)
        self._im_stats = tk.PhotoImage(master=self, file=IM_STATS)
        self._im_about = tk.PhotoImage(master=self, file=IM_ABOUT)
        self._im_help = tk.PhotoImage(master=self, file=IM_HELP)

        style = ttk.Style(self)
        style.theme_use('clam')
        style.configure('url.TLabel', foreground='blue')
        style.configure('flat.TButton', relief='flat')
        style.configure('flat.TMenubutton', relief='flat')
        style.configure('top.TFrame', relief='raised', padding=0, borderwidth=1)
        bg = style.lookup('TFrame', 'background', default='#ececec')
        self.configure(bg=bg)
        self.option_add('*Toplevel.background', bg)
        self.option_add('*Frame.background', bg)
        self.option_add('*Button.background', bg)
        self.option_add('*Text.background', bg)
        self.option_add('*Menu.background', bg)

        # --- matplotlib config
        font = Font(self, font='TkDefaultFont').actual()
        matplotlib.use("TkAgg")
        matplotlib.rc('font', size=font['size'], family=font['family'])
        matplotlib.rc('axes', titlesize=font['size'] + 2)

        # --- top bar
        top_frame = ttk.Frame(self, style='top.TFrame')
        self.lang_menu = tk.Menu(top_frame, tearoff=False)
        self.lang = tk.StringVar(self)
        for key, name in LANGUAGES.items():
            self.lang_menu.add_radiobutton(label=name, value=key,
                                           variable=self.lang,
                                           command=self.change_lang)
        self.lang.set(SETTINGS.get('General', 'language'))

        ttk.Menubutton(top_frame, text=_('Language'), style='flat.TMenubutton',
                       width=len(_('Language')),
                       menu=self.lang_menu).pack(side='left', padx=1, pady=2)

        ttk.Button(top_frame, image=self._im_about, padding=4, style='flat.TButton',
                   command=self.about).pack(side='right', padx=1, pady=2)

        # --- PPAs
        self.ppas = {}
        self.ppa_widgets = {}

        text = tk.Text(self, relief='flat', highlightthickness=0, width=1,
                       height=1, cursor='arrow')
        scroll_x = AutoScrollbar(self, orient='horizontal', command=text.xview)
        scroll_y = AutoScrollbar(self, orient='vertical', command=text.yview)
        text.configure(xscrollcommand=scroll_x.set, yscrollcommand=scroll_y.set)

        self.ppa_frame = ttk.Frame(text)

        for i, ppa in enumerate(CONFIG.sections()):
            self.ppas[ppa] = None
            l = ttk.Label(self.ppa_frame, text=ppa)
            b1 = ttk.Button(self.ppa_frame, image=self._im_stats, padding=1,
                            command=lambda p=ppa: self.show_stats(p))
            b2 = ttk.Button(self.ppa_frame, image=self._im_edit, padding=4,
                            command=lambda p=ppa: self.edit_ppa(p))
            b3 = ttk.Button(self.ppa_frame, image=self._im_del, padding=4,
                            command=lambda p=ppa: self.remove_ppa(p))
            self.ppa_widgets[ppa] = [l, b1, b2, b3]

            l.grid(row=i, column=0, sticky='e', padx=4, pady=4)
            b1.grid(row=i, column=1, sticky='w', padx=4, pady=4)
            b2.grid(row=i, column=2, sticky='w', padx=4, pady=4)
            b3.grid(row=i, column=3, sticky='w', padx=4, pady=4)

        text.window_create('1.0', window=self.ppa_frame)
        text.configure(state='disabled')

        b_add = ttk.Button(self, image=self._im_add, padding=4, command=self.edit_ppa)

        # --- grid
        top_frame.grid(row=0, columnspan=2, sticky='ew')
        text.grid(row=1, column=0, sticky='ewsn')
        scroll_y.grid(row=1, column=1, sticky='ns')
        scroll_x.grid(row=2, column=0, sticky='ew')
        b_add.grid(row=3, column=0, sticky='w', padx=4, pady=4)

        self.update_idletasks()
        self._pady = b_add.winfo_reqheight() + top_frame.winfo_reqheight() + scroll_x.winfo_reqheight() + 8
        self._padx = scroll_y.winfo_reqwidth() + 8
        self._resize()

    def change_lang(self):
        SETTINGS.set('General', 'language', self.lang.get())
        save_settings()
        showinfo(_("Information"),
                 _("The language setting will take effect after restarting the application"),
                 parent=self)

    def report_callback_exception(self, *args):
        """Log exceptions."""
        err = "".join(traceback.format_exception(*args))
        logging.error(err)

    def _resize(self):
        h = min(self.ppa_frame.winfo_reqheight() + self._pady,
                self.ppa_frame.winfo_screenheight())
        w = min(self.ppa_frame.winfo_reqwidth() + self._padx,
                self.ppa_frame.winfo_screenwidth())

        self.geometry('{}x{}'.format(w, h))

    def about(self):
        About(self)

    def quit(self):
        for after_id in self.tk.call('after', 'info'):
            if isinstance(after_id, str):
                self.after_cancel(after_id)
        logging.info('Closing ppastats')
        self.destroy()

    def show_stats(self, ppa):
        try:
            self.ppas[ppa].lift()
        except (AttributeError, tk.TclError):
            owner = CONFIG.get(ppa, 'owner')
            name = CONFIG.get(ppa, 'name')
            distributions = CONFIG.get(ppa, 'distributions').split(', ')
            archs = CONFIG.get(ppa, 'archs').split(', ')
            self.ppas[ppa] = PPAToplevel(owner, name, distributions, archs)

    def remove_ppa(self, ppa, confirmation=True):
        rep = True
        if confirmation:
            rep = askokcancel(_("Confirmation"),
                              _("Do you really want to delete {ppa_name}?").format(ppa_name=ppa))
        if rep:
            if self.ppas[ppa] is not None:
                self.ppas[ppa].quit()
            for w in self.ppa_widgets[ppa]:
                w.destroy()
            try:
                os.remove(os.path.join(PATH_DATA, ppa.replace('/', '_') + '.h5'))
            except FileNotFoundError:
                pass
            del self.ppas[ppa]
            del self.ppa_widgets[ppa]
            CONFIG.remove_section(ppa)
            save_config()
            if confirmation:
                logging.info('Removed PPA {}'.format(ppa))
                self.update_idletasks()
                self._resize()

    def edit_ppa(self, ppaname=None):

        def ok(event=None):
            owner = owner_entry.get()
            name = name_entry.get()
            distributions = distributions_entry.get().split(',')
            distributions = [d.strip() for d in distributions]
            while '' in distributions:
                distributions.remove('')
            archs = archs_entry.get().split(',')
            archs = [a.strip() for a in archs]
            while '' in archs:
                archs.remove('')
            if not owner or not name or not distributions or not archs:
                showerror(_('Error'), _('All fields are mandatory.'))
            else:
                create_widgets = False
                ppa = '{}/{}'.format(owner, name)
                if ppaname is not None:
                    old_owner = CONFIG.get(ppaname, 'owner')
                    old_name = CONFIG.get(ppaname, 'name')
                    if old_owner != owner or old_name != name:
                        self.remove_ppa(ppaname, confirmation=False)
                        CONFIG.add_section(ppa)
                        create_widgets = True
                else:
                    try:
                        CONFIG.add_section(ppa)
                        create_widgets = True
                    except configparser.DuplicateSectionError:
                        rep = askokcancel(_('Warning'),
                                          _('This PPA is already configured. Do you want to modify it?'),
                                          icon='warning')
                        if not rep:
                            top.destroy()
                            return
                CONFIG.set(ppa, 'owner', owner)
                CONFIG.set(ppa, 'name', name)
                CONFIG.set(ppa, 'distributions', ', '.join(distributions))
                CONFIG.set(ppa, 'archs', ', '.join(archs))
                save_config()

                if create_widgets:
                    self.ppas[ppa] = None
                    i = self.ppa_frame.grid_size()[1]
                    l = ttk.Label(self.ppa_frame, text=ppa)
                    b1 = ttk.Button(self.ppa_frame, image=self._im_stats, padding=1,
                                    command=lambda p=ppa: self.show_stats(p))
                    b2 = ttk.Button(self.ppa_frame, image=self._im_edit, padding=4,
                                    command=lambda p=ppa: self.edit_ppa(p))
                    b3 = ttk.Button(self.ppa_frame, image=self._im_del, padding=4,
                                    command=lambda p=ppa: self.remove_ppa(p))
                    self.ppa_widgets[ppa] = [l, b1, b2, b3]

                    l.grid(row=i, column=0, sticky='e', padx=4, pady=4)
                    b1.grid(row=i, column=1, sticky='w', padx=4, pady=4)
                    b2.grid(row=i, column=2, sticky='w', padx=4, pady=4)
                    b3.grid(row=i, column=3, sticky='w', padx=4, pady=4)
                    self.update_idletasks()
                    self._resize()

                    logging.info('Configured PPA {}'.format(ppa))

                top.destroy()

        top = tk.Toplevel(self)
        top.title(_('Configure PPA'))
        top.columnconfigure(1, weight=1)
        top.transient(self)
        top.grab_set()

        owner_entry = ttk.Entry(top, width=30)
        name_entry = ttk.Entry(top, width=30)
        distributions_entry = ttk.Entry(top, width=30)
        archs_entry = ttk.Entry(top, width=30)

        if ppaname is not None:
            owner = CONFIG.get(ppaname, 'owner')
            name = CONFIG.get(ppaname, 'name')
            distributions = CONFIG.get(ppaname, 'distributions')
            archs = CONFIG.get(ppaname, 'archs')
            owner_entry.insert(0, owner)
            name_entry.insert(0, name)
            distributions_entry.insert(0, distributions)
            archs_entry.insert(0, archs)

        ttk.Label(top, text=_('PPA owner:')).grid(row=0, column=0, padx=4, pady=4, sticky='e')
        ttk.Label(top, text=_('PPA name:')).grid(row=1, column=0, padx=4, pady=4, sticky='e')
        ttk.Label(top, text=_('Distributions:')).grid(row=2, column=0, padx=4, pady=4, sticky='e')
        ttk.Label(top, text=_('Architectures:')).grid(row=3, column=0, padx=4, pady=4, sticky='e')
        owner_entry.grid(row=0, column=1, padx=4, pady=4, sticky='we')
        name_entry.grid(row=1, column=1, padx=4, pady=4, sticky='we')
        distributions_entry.grid(row=2, column=1, padx=4, pady=4, sticky='we')
        archs_entry.grid(row=3, column=1, padx=4, pady=4, sticky='we')

        frame = ttk.Frame(top)

        ttk.Button(frame, text=_('Ok'),
                   command=ok).grid(row=0, column=1, padx=4, pady=4, sticky='e')
        ttk.Button(frame, text=_('Cancel'),
                   command=top.destroy).grid(row=0, column=2, padx=4, pady=4, sticky='w')
        bhelp = ttk.Button(frame, image=self._im_help, command=self.help, padding=4)
        bhelp.grid(row=0, column=3, padx=4, pady=4, sticky='e')
        top.update_idletasks()
        frame.columnconfigure(0, minsize=bhelp.winfo_reqwidth())
        frame.columnconfigure(1, weight=1)
        frame.columnconfigure(2, weight=1)
        frame.grid(row=4, columnspan=2, sticky='ew')

        owner_entry.bind('<Return>', ok)
        name_entry.bind('<Return>', ok)
        owner_entry.bind('<Return>', ok)
        archs_entry.bind('<Return>', ok)
        owner_entry.focus_set()

    def help(self):
        Help(self)
