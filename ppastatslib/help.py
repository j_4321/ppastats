#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
ppastats - Launchpad PPA stats viewer
Copyright 2016-2018 Juliette Monsel <j_4321@protonmail.com>

ppastats is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ppastats is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Help dialog
"""


from tkinter import Toplevel, PhotoImage
from tkinter.ttk import Button, Label
from ppastatslib.constants import IM_HELP_48


class Help(Toplevel):
    """Help Toplevel."""
    def __init__(self, master):
        """Create the Toplevel."""
        Toplevel.__init__(self, master, padx=6, pady=6)
        self.transient(master)
        self.title(_("Help"))
        self._im_help = PhotoImage(master=self, file=IM_HELP_48)
        b = Button(self, text=_("Close"), command=self.destroy)

        Label(self, image=self._im_help).grid(rowspan=4, column=0, stick='w',
                                              padx=4, pady=4)
        Label(self, text=_('PPA owner:'),
              font='TkDefaultFont 9 bold').grid(row=0, column=1, padx=4, pady=4, sticky='e')
        Label(self,
              text=_('Launchpad username.')).grid(row=0, column=2, padx=4, pady=4, sticky='w')
        Label(self, text=_('Distributions:'),
              font='TkDefaultFont 9 bold').grid(row=1, column=1, padx=4, pady=4, sticky='e')
        Label(self,
              text=_('Comma separated list of Ubuntu distributions, e.g. "xenial, bionic".')).grid(row=1,
                                                                                                   column=2,
                                                                                                   padx=4,
                                                                                                   pady=4,
                                                                                                   sticky='w')
        Label(self, text=_('Architectures:'),
              font='TkDefaultFont 9 bold').grid(row=2, column=1, padx=4, pady=4, sticky='e')
        Label(self,
              text=_('Comma separated list of architectures, e.g. "amd64, i386".')).grid(row=2,
                                                                                         column=2,
                                                                                         padx=4,
                                                                                         pady=4,
                                                                                         sticky='w')
        b.grid(row=4, columnspan=3, pady=4, padx=4)

        self.resizable(0, 0)
        b.focus_set()
        self.grab_set()
