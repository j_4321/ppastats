#! /usr/bin/python3
# -*- coding: utf-8 -*-
# PEP8: ignore = E402,E501
"""
ppastats - Launchpad PPA stats viewer
Copyright 2018 Juliette Monsel <j_4321@protonmail.com>

ppastats is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ppastats is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Page displaying the stats for one package
"""

import tkinter as tk
from tkinter import ttk
from ppastatslib.utils import Event, AutoScrollbar
from ppastatslib.navtoolbar import NavigationToolbar
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import pandas as pd


class Page(ttk.Frame):
    def __init__(self, master, pagetype='package'):
        ttk.Frame.__init__(self, master)

        self._type = pagetype  # package or PPA

        paned = ttk.Panedwindow(self, orient='horizontal')

        style = ttk.Style(self)
        bg = style.lookup('TFrame', 'background', default='#dcdad5')
        if not isinstance(bg, str):
            bg = '#dcdad5'

        # --- downloads by version
        frame_1 = ttk.Frame(paned)
        frame_1.columnconfigure(0, weight=1)
        frame_1.rowconfigure(1, weight=1)
        self.total = ttk.Label(frame_1, font='TkDefaultFont 11')
        self.can = tk.Canvas(frame_1, bg=bg, relief='flat', highlightthickness=0)
        frame_fig = ttk.Frame(self.can)
        sy = AutoScrollbar(frame_1, orient='vertical', command=self.can.yview)
        self.can.configure(yscrollcommand=sy.set)
        self.can.grid(row=1, column=0, sticky='ewsn')
        sy.grid(row=1, column=1, sticky='sn')
        self.total.grid(row=0, columnspan=2, sticky='w', padx=4, pady=4)

        self.fig_version = Figure(figsize=(2, 5), dpi=100, facecolor=bg)
        self.ax_version = self.fig_version.add_subplot(1, 1, 1)
        self.figAgg_version = FigureCanvasTkAgg(self.fig_version, frame_fig)
        self.figAgg_version.draw()
        self.figAgg_version.get_tk_widget().pack(fill='both', expand=True)
        self.can.create_window(0, 0, anchor='nw', tags='fig',
                               window=frame_fig)
        self.figAgg_version.get_tk_widget().configure(bg=bg)
        self.figAgg_version.get_tk_widget().bind('<Configure>', self._resize_fig)
        self.figAgg_version.get_tk_widget().bind('<Button-4>', lambda e: self._mouse_scroll(-1))
        self.figAgg_version.get_tk_widget().bind('<Button-5>', lambda e: self._mouse_scroll(1))
        self.can.bind('<Configure>', self._resize_can)
        self._fig_version_height = 0

        # --- downloads by date
        frame_2 = ttk.Frame(paned)
        self.fig_date = Figure(figsize=(6.5, 5), dpi=100, facecolor=bg)
        self.ax_daily = self.fig_date.add_subplot(2, 1, 1)
        self.ax_monthly = self.fig_date.add_subplot(2, 1, 2)
        self.figAgg_date = FigureCanvasTkAgg(self.fig_date, frame_2)
        self.toolbar = NavigationToolbar(self.figAgg_date, frame_2, self.tight_layout)
        self.figAgg_date.draw()
        self.figAgg_date.get_tk_widget().configure(bg=bg)
        self.figAgg_date.get_tk_widget().pack(fill='both', expand=True)

        paned.add(frame_1)
        paned.add(frame_2)
        paned.pack(fill='both', expand=True)

    def _mouse_scroll(self, delta):
        self.can.yview_scroll(delta, 'units')

    def plot(self, version_data, time_data):
        self.ax_version.clear()
        self.ax_daily.clear()
        self.ax_monthly.clear()
        try:
            if self._type == 'package':
                title = _('Per Version')
            else:
                title = _('Per Package')
            version_data.plot(kind='barh', ax=self.ax_version, legend=False,
                              title=title)
            self._fig_version_height = len(version_data['Downloads']) * 25 + 80
            self.ax_version.set_xlabel(_('Downloads'))
            self.ax_version.set_ylabel('')
            self.total.configure(text=_('Total downloads: {nb}').format(nb=version_data['Downloads'].sum()))
            daily = time_data.reindex(time_data.resample('D').asfreq().index, fill_value=0)
            monthly = daily.groupby(pd.Grouper(freq="M")).sum()
            daily.plot(ax=self.ax_daily, legend=False, title=_('Per Day'))
            xticks = [i.strftime('%b %Y') for i in monthly.index]
            monthly.plot(kind='bar', ax=self.ax_monthly, legend=False, title=_('Per Month'))
            self.ax_monthly.set_xlabel('')
            self.ax_monthly.set_ylabel(_('Downloads'))
            self.ax_monthly.set_xticklabels(xticks)
            self.ax_monthly.tick_params('x', rotation=70)
            self.ax_daily.set_xlabel('')
            self.ax_daily.set_ylabel(_('Downloads'))
        except TypeError:
            pass
        self.figAgg_version.draw_idle()
        self.figAgg_date.draw_idle()
        self._resize_fig()
        self.tight_layout()
        self._resize_can()
        self.after(10, self.tight_layout)

    def tight_layout(self):
        try:
            self.fig_version.tight_layout(pad=1)
            self.fig_date.tight_layout(pad=1)
        except Exception:
            pass
        self.figAgg_version.draw_idle()
        self.figAgg_date.draw_idle()

    def _resize_can(self, event=None):
        try:
            width = self.can.winfo_width()
            self.can.itemconfigure('fig', width=width, height=self._fig_version_height + self.toolbar.winfo_height())
            self.figAgg_version.resize(Event(width=width, height=self._fig_version_height))
            try:
                self.fig_version.tight_layout(pad=1)
            except Exception:
                pass
            self.figAgg_version.draw_idle()
        except tk.TclError:
            # error triggered when destroying the app
            pass

    def _resize_fig(self, event=None):
        self.can.configure(scrollregion=self.can.bbox('all'))
